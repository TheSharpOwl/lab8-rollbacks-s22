import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=admin user=admin")

get_stock = r"""
    SELECT SUM(amount)
    FROM Inventory
    WHERE username = %(username)s
"""

buy_cmd = r"""
    UPDATE Player
    SET balance = balance - (
            SELECT price
            FROM Shop
            WHERE product = %(product)s
        ) * %(amount)s
    WHERE username = %(username)s
"""

upd_inv = r"""
    UPDATE Inventory
    SET amount = amount + %(amount)s
    WHERE username = %(username)s AND product = %(product)s
"""

buy_and_dec_stock = r"""
    UPDATE Shop
    SET in_stock = in_stock - %(amount)s
    WHERE product = %(product)s
"""




def buy(username, product, amount):
    with conn:
        with conn.cursor() as c:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                c.execute(buy_cmd, obj)
                if c.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                c.execute(buy_and_dec_stock, obj)
                if c.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            c.execute(get_stock, obj)
            inventory_amount = c.fetchone()[0]
            if inventory_amount + amount > 100:
                raise Exception("Inventory is full")

            c.execute(upd_inv, obj)

            conn.commit()
